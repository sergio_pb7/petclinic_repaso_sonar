package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTest {

	@Autowired
	private PetRepository pets;
	
	@Autowired
	private OwnerRepository owners;
	
	private Pet pet;

	
	@Before
	public void init() {
		if (this.pet==null) {
			pet = new Pet();
			
			Owner owner = new Owner();
			owner.setAddress("Direccion");
			owner.setCity("Ciudad");
			owner.setFirstName("Nombre");
			owner.setLastName("Apellido");
			owner.setTelephone("999999999");
			owners.save(owner);
			
			pet.setName("Mascota");
			pet.setBirthDate(LocalDate.of(2019, 5, 6));
			PetType petType = this.pets.findPetTypes().get(0);
			pet.setType(petType);
			
			owner.addPet(pet);
			pets.save(pet);
			
	
		}
	}
	
	@Test
	@Transactional
	public void testSave() {
		pet = new Pet();
		
		Owner owner = new Owner();
		owner.setAddress("Direccion");
		owner.setCity("Ciudad");
		owner.setFirstName("Nombre");
		owner.setLastName("Apellido");
		owner.setTelephone("999999999");
		owners.save(owner);
		Pet pet = new Pet();
		pet.setBirthDate(LocalDate.of(2019, 5, 6));
		PetType petType = this.pets.findPetTypes().get(0);
		pet.setType(petType);
		
		owner.addPet(pet);
		
		assertNull(pet.getId());
		pets.save(pet);	
		assertNotNull(pet.getId());
		
	}
	@Test
	public void testDelete() {
		
		assertNotNull(pet.getId());
		pets.delete(pet);
		assertNull(pets.findById(pet.getId()));
		
		
	}
	@Test
	public void testFindByName() {
		assertNotNull(pet.getId());
		Collection<Pet> petsCol = new LinkedList<Pet>();
		petsCol.add(pet);
		Collection<Pet> petsCollect = pets.findPetsByName("Mascota");
		assertArrayEquals(petsCollect.toArray(),petsCol.toArray());
		petsCollect = pets.findPetsByName("H");
		assertNotEquals(petsCollect.toArray(),petsCol.toArray());
		
	}
	@After
	public void finish() {
		this.pet=null;
	}

}

