package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTest {

	@Autowired
	private VisitRepository visits;
	
	@Autowired
	private PetRepository pets;
	
	@Autowired
	private OwnerRepository owners;
	
	private Visit visit;

	
	@Before
	public void init() {
		if (this.visit==null) {
			visit = new Visit();
			
			Owner owner = new Owner();
			owner.setAddress("Direccion");
			owner.setCity("Ciudad");
			owner.setFirstName("Nombre");
			owner.setLastName("Apellido");
			owner.setTelephone("999999999");
			owners.save(owner);
			Pet pet = new Pet();
			pet.setBirthDate(LocalDate.of(2019, 5, 6));
			PetType petType = this.pets.findPetTypes().get(0);
			pet.setType(petType);
			
			owner.addPet(pet);
			pets.save(pet);
			
			
			visit.setDescription("VisitaTest");
			visit.setPetId(pet.getId());
			visit.setDate(LocalDate.of(2019, 5, 6));
	
			
			visits.save(visit);	
		}
	}
	
	@Test
	@Transactional
	public void testSave() {
		Visit visit = new Visit();
		
		Owner owner = new Owner();
		owner.setAddress("Direccion");
		owner.setCity("Ciudad");
		owner.setFirstName("Nombre");
		owner.setLastName("Apellido");
		owner.setTelephone("999999999");
		owners.save(owner);
		Pet pet = new Pet();
		pet.setBirthDate(LocalDate.of(2019, 5, 6));
		PetType petType = this.pets.findPetTypes().get(0);
		pet.setType(petType);
		
		owner.addPet(pet);
		pets.save(pet);
		
		
		visit.setDescription("VisitaTest");
		visit.setPetId(pet.getId());
		visit.setDate(LocalDate.of(2019, 5, 6));

		assertNull(visit.getId());
		visits.save(visit);	
		assertNotNull(visit.getId());
		
	}
	@Test
	public void testUpdate() {
		
	
		visit.setDescription("VisitaTestModificada");
		visits.save(visit);
		Visit visitFind = visits.findById(visit.getId());
		assertEquals(visitFind.getDescription(), "VisitaTestModificada");
		
	}
	@After
	public void finish() {
		this.visit=null;
	}

}
