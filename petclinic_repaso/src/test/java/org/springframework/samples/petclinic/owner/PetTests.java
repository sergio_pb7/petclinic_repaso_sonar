package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PetTests {

	public static Pet pet;
	
	@BeforeClass
	public static void initClass() {
		pet = new Pet();
		pet.setName("Mascota");
	}
	@Before
	public void init() {
		pet.setWeight((float) 1.3);
		pet.setComments("Comentario");
	}
	@Test
	public void testWeight() {
		assertNotNull(pet.getWeight());
		assertTrue(pet.getWeight() == (float)1.3);
		pet.setWeight((float) 2.5);
		assertTrue(pet.getWeight() == (float)2.5);
	}
	@Test
	public void testComments() {
		assertNotNull(pet.getComments());
		assertEquals(pet.getComments(), "Comentario");
		pet.setComments("ComentarioNuevo");
		assertEquals(pet.getComments(), "ComentarioNuevo");
	}
	@After
	public void finish() {
		pet.setWeight((float) 1.3);
		pet.setComments("Comentario");
	}
	@AfterClass
	public static void finishClass() {
		pet = null;
	}
}
